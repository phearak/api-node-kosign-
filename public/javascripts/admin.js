(function($) {
    //----- OPEN
    $('[data-popup-open]').on('click', function(e)  {
        var targeted_popup_class = jQuery(this).attr('data-popup-open');
        $('[data-popup="' + targeted_popup_class + '"]').fadeIn();
        e.preventDefault();
    });
    //----- CLOSE
    $('[data-popup-close]').on('click', function(e)  {
        var targeted_popup_class = jQuery(this).attr('data-popup-close');
        $('[data-popup="' + targeted_popup_class + '"]').fadeOut(100);
        e.preventDefault();
    });

    // Button Create New API
    $('#new_api').on('submit', function(event) {
        //alert(1);
        $.post('./views/admin.jade', $('#new_api').serialize, function(data) {
            alert(data);
        });
        
        return true;
        event.preventDefault();
    });
    
    // Button New API
    $('#create_api').on('submit', function(event) {
        
        //alert("btn new api");
        var result = $('#new_api').val();
        $('#create_api').find('#get_name').val($('#new_api').val());
        console.log("All data get from: "+ result);
        
        return true;
        event.preventDefault();
    });

    // Onload show form login
    // $('#login, h4').hide();
    // $('.alert.alert-danger.alert-dismissable.fade.in').hide();

    // $('#btn_cnewapi').on('click', function(e) {
    //     e.preventDefault();
    //     $('#login, h4').slideToggle(1000);
    //     $('.alert.alert-danger.alert-dismissable.fade.in').slideToggle(1000);
    // });

    //$('#enquirypopup').hide();

    $('#btn_relog').on('click', function(e) {
        e.preventDefault();
        alert("Hello relog");
    });

    var username, password;
    var _name;
    $('#adlogin').click(function() {
        alert("H");
        var user = $('#username').val();
        var pass = $('#password').val();

        $("#profiles").text(user);
        _name = $('#username').val();
        console.log(user);
        $.post("http://localhost:3000/login", {username: user, password: pass}, function(data) {
            if(data == 'done') {
                window.location.href="/admin";
            }
        });
    });

    $('li.list-group-item a').click(function(event) {       
        event.preventDefault();
		console.log("TEST1: ", $(this).attr("attr"));
        $('#new_api').find('#data-attr').val($(this).attr("attr"));
       
       /*$.post('api_u001', {"SOK":"SAO"}, function(result, text) {
            console.log(params);

        });
        // Complete, runs on error and success
        .complete(function(result, text) {
           
        });
        .error(function(err, text) {
           console.log(err);
        });*/
    });
    
    // Auth with OAuth
    $('#credentials').submit(function(event) {
        event.preventDefault();
        var params = $(this).serializeArray();
        $('#oauthAuthenticated').hide();
        $('section.credentials').removeClass('authed');
        if (params[1].name == 'oauth') {
            $.post('auth', params, function(result) {
                if (result.signin) {
                    window.open(result.signin,"_blank","height=900,width=800,menubar=0,resizable=1,scrollbars=1,status=0,titlebar=0,toolbar=0");
                }
            })
        } else if (params[1].name == 'oauth2') {
            $.post('auth2', params, function(result) {
                if (result.signin) {
                    window.open(result.signin,"_blank","height=900,width=800,menubar=0,resizable=1,scrollbars=1,status=0,titlebar=0,toolbar=0");
                }
                else if (result.implicit) {
                    window.open(result.implicit,"_blank","height=900,width=800,menubar=0,resizable=1,scrollbars=1,status=0,titlebar=0,toolbar=0");
                }
                else if (result.refresh) {
                    window.open(result.refresh,"_blank","height=900,width=800,menubar=0,resizable=1,scrollbars=1,status=0,titlebar=0,toolbar=0");
                }
                else {
                    window.location.reload();
                }
            })
        }
    });
    
    // $.('#access_token').val(foo);


    /*
        Try it! button. Submits the method params, apikey and secret if any, and apiName
    */
    $('li.method form').submit(function(event) {
        var self = this;

        event.preventDefault();

        var params = $(this).serializeArray(),
            apiKey = { name: 'apiKey', value: $('input[name=key]').val() },
            apiSecret = { name: 'apiSecret', value: $('input[name=secret]').val() },
            apiName = { name: 'apiName', value: $('input[name=apiName]').val() },
            apiUsername = { name: 'apiUsername', value: $('input[name=username]').val() },
            apiPassword = { name: 'apiPassword', value: $('input[name=password]').val() };;

        params.push(apiKey, apiSecret, apiName, apiUsername, apiPassword);

        //Accounts for array values
        for (i in params) {
            if (params[i].name.split("_")[0] == "values") {
                params[i].name = params[i].name.split("_")[0] + "[" + params[i].name.split("_")[1] + "]"; 
            }
        }

        // Setup results container
        var resultContainer = $('.result', self);
        if (resultContainer.length === 0) {
            resultContainer = $(document.createElement('div')).attr('class', 'result');
            $(self).append(resultContainer);
        }

        if ($('pre.response', resultContainer).length === 0) {

            // Clear results link
            $(document.createElement('a'))
                .text('Clear results')
                .addClass('clear-results')
                .attr('href', '#')
                .click(function(e) {
                    e.preventDefault();

                    var thislink = this;
                    $('.result', self)
                        .slideUp(function() {
                            $(this).remove();
                            $(thislink).remove();
                        });
                })
                .insertAfter($('input[type=submit]', self));

            // Call that was made, add pre elements
            resultContainer.append($(document.createElement('h4')).text('Call'));
            resultContainer.append($(document.createElement('pre')).addClass('call'));

            // Request Headers
            resultContainer.append($(document.createElement('h4')).addClass('reqHeadText').text('Request Headers'));
            resultContainer.append($(document.createElement('pre')).addClass('requestHeaders'));

            // Request Body
            resultContainer.append($(document.createElement('h4')).addClass('reqBodyText').text('Request Body'));
            resultContainer.append($(document.createElement('pre')).addClass('requestBody'));

            // Code
            resultContainer.append($(document.createElement('h4')).text('Response Code'));
            resultContainer.append($(document.createElement('pre')).addClass('code prettyprint'));

            // Header
            resultContainer.append($(document.createElement('h4')).text('Response Headers'));
            resultContainer.append($(document.createElement('pre')).addClass('headers prettyprint'));

            // Response
            resultContainer.append($(document.createElement('h4'))
                .text('Response Body')
                .append($(document.createElement('a'))
                    .text('Select body')
                    .addClass('select-all')
                    .attr('href', '#')
                    .click(function(e) {
                        e.preventDefault();
                        selectElementText($(this.parentNode).siblings('.response')[0]);
                    })
                )
            );

            resultContainer.append($(document.createElement('pre'))
                .addClass('response prettyprint'));
        }

        $.post('api_u', params, function(result, text) {
            console.log(params);

        })
        // Complete, runs on error and success
        .complete(function(result, text) {
           
        })
        .error(function(err, text) {
           
        })
    })

})(jQuery);
