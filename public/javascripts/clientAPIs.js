$(function() {
    //----- OPEN
    $('[data-popup-open]').on('click', function(e)  {
        var targeted_popup_class = jQuery(this).attr('data-popup-open');
        $('[data-popup="' + targeted_popup_class + '"]').fadeIn();
        e.preventDefault();
    });
    //----- CLOSE
    $('[data-popup-close]').on('click', function(e)  {
        var targeted_popup_class = jQuery(this).attr('data-popup-close');
        $('[data-popup="' + targeted_popup_class + '"]').fadeOut(100);
        e.preventDefault();
    });
    
    // button login admin
    $('#btnlogin').on('click', function() {
        
    });
    
    $('#btnSwitch').click(function() {
        $('#btnlogin').slideDown();
    });
    
    // load form popup model
    $('#onload').find('#password').focus();
    $('#username').focus();
    $('#admimpage').hide();

    // button submit login show Custom Alert
    $('#evevtlog').on('submit', function(event) {
        
        var f_username = $('#username').val();
        var f_password = $('#password').val();

        $('#username').focus(); 

        $('#nameuser').find('#profile').html(f_username);
        $('#nameusers').find('#profiles').html(f_username);

        if(f_username === "" && f_password === "" || f_username ==="" || f_password ==="") {
            $('#demo_error').text("Your fields are not matched!...").show().fadeOut(1500).css({"margin-left":"10px", "font-size":"14px", "color":"red"});
        }
        return true;
        event.preventDefault();
    });

    // when check combobox remember button 
    var username = $('#username').val();
    var password = $('#password').val();
    
    // check remember me
    if(localStorage.rememberme && localStorage.rememberme != '') {
        $('#rememberme').attr('checked', 'checked');
        $('#username').val(localStorage.username);
        $('#password').val(localStorage.password);
    }
    else{
        $('#rememberme').removeAttr('checked');
        $('#username').val('');
        $('#password').val('');

        $('#username').focus();
    }

    $('#rememberme').click(function() {
        if($('#rememberme').is(':checked')) {
            // save username and password
            localStorage.username = $('#username').val();
            localStorage.rememberme = $('#rememberme').val();
            $('#password').focus();
        }
        else {
            localStorage.username = '';
            localStorage.password = '';
            localStorage.rememberme = '';
            $('#username').focus();
        }
    });
    
    $('#btn_Login').click(function(e) {
        $('#profile').text($("#username").val());
    });
});